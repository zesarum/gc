const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const pug = require('gulp-pug')
var cleanCSS = require('gulp-clean-css');
var htmlmin = require('gulp-htmlmin');
var browserSync = require('browser-sync').create();

var reload      = browserSync.reload;

// gulp.task('default', () =>
//     gulp.src('images/*/*')
//         .pipe(imagemin())
//         .pipe(gulp.dest('dist/images'))
// );


gulp.task('default', () => {
  gulp.watch('./dev/views/**/*.pug', ['pug']);
});


gulp.task('minify-css', function() {
  return gulp.src('css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css'));
});


gulp.task('minify', function() {
  return gulp.src('index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist/'));
});

gulp.task('pug', () =>
  gulp.src('./dev/views/*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./dist/'))
);

// Save a reference to the `reload` method

// Watch scss AND html files, doing different things with each.
gulp.task('serve', function () {

    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: "dist/"
        }
    });

    gulp.watch("./dist/**/*.html").on("change", reload);
});

gulp.task('server', function () {

    // Serve files from the root of this project
    browserSync.init({
        port: 8010,
        server: {
            baseDir: "./"
        }
    });

    gulp.watch(["*.html", "./css/*.css", "./images/*.jpg"]).on("change", reload);
});
